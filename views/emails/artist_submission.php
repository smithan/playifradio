<html>
<head>
<style type="text/css">
    p { 
        font-family: 'Droid Sans', Helvetica, Arial; 
    } a {
        color: #1BA2CF;
    }
</style>
</head>
<body>
<table border=0 width=500>
<tr><td>
<div align=right>
    <img src="http://playifradio.com/views/img/pir-logo.png" alt="">
</div>
<p><strong>Hello!</strong></p>
<p>This is an automatically generated email to let you know
that your PlayIf Radio Beta Artist submission has been 
succesfully processed.  If you would like to add some music to PlayIf Radio, please
go to this link:  
<a href="http://playifradio.com/register/login/">http://playifradio.com/register/login/</a></p>
<p>Thank you for your support.  If you have any questions, don't
hesitate to email us at <a href="mailto:support@playifradio.com">support@playifradio.com</a></p>
<p>
Sincerely,<br>
<strong>PlayIf Radio Staff<strong><br>
<a href="http://playifradio.com">http://playifradio.com/</a><br>
</p>
</td></tr>
</table>
</body>
</html>
