<meta charset="utf-8">
<title>
    PlayIf Radio 
    <?php 
        if (isset($title)) 
            echo " | " . $title;
    ?>
</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" value="PlayIf Radio is a new way to share and discover underground and indie music">
<meta name="keywords" value="PlayIf Radio, indie music, underground music, indie radio, search by artist, discover indie music">
<link rel="stylesheet" href="<?=BASE_URL?>views/css/normalize-min.css">
<link rel="stylesheet" href="<?=BASE_URL?>views/css/registration-min.css">
<link rel="icon" href="<?=BASE_URL?>favicon.ico">
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>

